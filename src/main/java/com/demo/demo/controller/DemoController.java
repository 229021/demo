package com.demo.demo.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

/**
 * @Author zhouzhongqing
 * @Date 2021/2/4 10:48
 * @Version 1.0
 **/
@RestController
@Slf4j
@RequestMapping("/demo")
public class DemoController {

    /**
     * 查询
     *
     * @return java.lang.String
     * @Author: zhouzhongqing
     * @Date 2021/2/4 10:49
     */
    @RequestMapping("query")
    public String query() {
        log.info("query...");
        String str = "hello world！测试";
        log.info("query...OK,结果={}", str);
        System.out.println("测试合并");
        return str;
    }
}
